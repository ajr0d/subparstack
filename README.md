# Subparstack

This project was tongue in cheek to see if I was still capable of writing
JavaScript, it's not to be taken too seriously.  The name is a joke on
Substack, the much better version of Subparstack, hence the name. The idea is
with some know-how, you can spin up your own blog. I have since decided to
combat web bloat and optimize speeds. I want to rewrite this without any
JavaScript whatsoever.  So hopefully Superiorstack comes out soon?

The only things you need is a [PostgreSQL](https://www.postgresql.org/) database, and an `.env` file.  Your file should look like this:
```.env
DB_USER=username
DB_HOST=host
DB_NAME=whatever
DB_PORT=0000
PORT=9999
```

You just run `node server.js` and hopefully it should work.  If not, I'd say
contact me, but this project isn't really worth it.  It's subpar, use Substack.
