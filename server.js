const express = require("express");
const { Pool } = require("pg");
const path = require("path");
const marked = require("marked");
const helmet = require("helmet");
const morgan = require("morgan");
const session = require("express-session");
const NodeCache = require("node-cache");
require("dotenv").config();

const app = express();
const port = process.env.PORT || 3000;

// Middleware
app.use(helmet());
app.use(morgan("combined"));
app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname, "public")));
app.use(express.urlencoded({ extended: true }));
app.use(
  session({
    secret: "your_secret_key",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false },
  }),
);

app.use((req, res, next) => {
  res.locals.isAdmin = req.session.isAdmin;
  next();
});

const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  port: process.env.DB_PORT,
});

pool.on("connect", () => {
  console.log("Connected to the database");
});

pool.on("error", (err) => {
  console.error("Unexpected error on idle client", err);
  process.exit(-1);
});

const cache = new NodeCache({ stdTTL: 100, checkperiod: 120 });

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/about", (req, res) => {
  res.render("about");
});

app.get("/login", (req, res) => {
  res.render("login");
});

app.post("/login", (req, res) => {
  const { username, password } = req.body;
  if (username === "admin" && password === "password") {
    req.session.isAdmin = true;
    res.redirect("/blog/create");
  } else {
    res.status(401).send("Unauthorized");
  }
});

app.get("/logout", (req, res) => {
  req.session.destroy();
  res.redirect("/");
});

app.get("/blog", async (req, res, next) => {
  const limit = 2;
  const page = req.query.page ? parseInt(req.query.page) : 1;
  const offset = (page - 1) * limit;

  const cacheKey = `blog_page_${page}`;

  if (cache.has(cacheKey)) {
    console.log("Cache hit for", cacheKey);
    const { blogPosts, totalPages } = cache.get(cacheKey);
    return res.render("blog", {
      blogPosts,
      currentPage: page,
      totalPages,
    });
  }

  try {
    const result = await pool.query(
      "SELECT * FROM blog_posts ORDER BY created_at DESC LIMIT $1 OFFSET $2",
      [limit, offset],
    );
    const blogPosts = result.rows;

    // Get total count of posts
    const countResult = await pool.query("SELECT COUNT(*) FROM blog_posts");
    const totalPosts = parseInt(countResult.rows[0].count, 10);
    const totalPages = Math.ceil(totalPosts / limit);

    // Store the result in cache
    cache.set(cacheKey, { blogPosts, totalPages });

    res.render("blog", {
      blogPosts: blogPosts,
      currentPage: page,
      totalPages: totalPages,
    });
  } catch (err) {
    console.error("Error executing query", err);
    next(err);
  }
});

app.get("/blog/:id", async (req, res, next) => {
  const postId = req.params.id;

  try {
    const result = await pool.query("SELECT * FROM blog_posts WHERE id = $1", [
      postId,
    ]);
    const post = result.rows[0];

    if (!post) {
      return res.status(404);
    }

    res.render("post", { post });
  } catch (err) {
    console.error("Error expecting query", err);
    next(err);
  }
});

function authMiddleware(req, res, next) {
  if (req.session.isAdmin) {
    next();
  } else {
    res.status(403).send("Forbidden");
  }
}

app.get("/blog/create", authMiddleware, (req, res) => {
  res.render("create-post", { isAdmin: req.session.isAdmin });
});

app.post("/blog/create", authMiddleware, async (req, res, next) => {
  const { title, content } = req.body;
  const htmlContent = marked.parse(content);

  try {
    await pool.query(
      "INSERT INTO blog_posts (title, content, html_content) VALUES ($1, $2, $3)",
      [title, content, htmlContent],
    );

    // Clear cache
    cache.flushAll();

    res.redirect("/blog");
  } catch (err) {
    console.error("Error executing query", err);
    next(err);
  }
});

// Catch-all route to handle other requests
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

// Centralized error handling middleware
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Something broke!");
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
